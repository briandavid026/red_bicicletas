var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
const bicicleta = require('../../models/bicicleta');

var base_url = 'http://localhost:5000/api/bicicletas';


describe('Bicicleta API', () => {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true });

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function() {
                console.log('we are connected to test database');
                done();
            });

    
        });    
    
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });    


    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            Request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicleta.length).toBe(0);
                done();
            });
        });         
    });      
            

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbano": "lat": -34, "lng": -54 }';
            request.post({
                headers: headers,
                url:     base_url + '/create',
                body:    aBici 
            },   function(error, response, body) {
                 expect(response.statusCode).toBe(200);
                 var bici = JSON.parse(body).bicicleta;
                 console.log(bici);
                 expect(bici.color).toBe("rojo");
                 expect(bici.ubicacion[0]).toBe(-34);
                 expect(bici.ubicacion[1]).toBe(-54);
                 done();
            });
        });
    });
});
    describe('DELETE BICICLETAS /delete' () => {
        it('Status 204', (done) => {
            var a = Bicicleta.createInstance(1, 'negro', 'urbana', [3.436915, -76.470492]);
            Bicicleta.add(a, function(err, newBici){
                var headers = {'content-type' : 'application/json'};
                var aBici = '{"code": 10}'
                request.delete({
                    headers: headers
                    url:     base_url + '/eliminar',
                    body:    aBici 
                }, function (error, response, body) {
                    expect(response.statusCode).toBe(204);
                    Bicicleta.allBicis (function (err, bici){
                        if(err) console.log (err);
                        expect(bici.length).toBe(0);
                        done();
                   }): 
                });
            });    
        });
    });
});        